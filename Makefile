tools := dont has_internet hold must should toggle is_up
installed_tools := $(patsubst %,$(PREFIX)/usr/bin/%,$(tools))

install: $(installed_tools)

$(PREFIX)/usr/bin/%: %
	install -m 755 -o root -g root $< $@
