Should a task run?
==================

Run tasks only when conditions are met.

Specially useful for cronjobs where you don't want to receive failure
emails when there are temporary errors.

It acts as a service controller for cronjobs.

Tools
-----

`should` asks if a certain topic is met.

```sh
# Only run rsync when synchronization is enabled
should sync && rsync -av origin/ dest/
```

`must` enables a topic.

```sh
must sync
```

`dont` disables a topic.

```sh
dont sync
```

`hold` pauses a task while it runs.

```sh
hold sync rsync -av alternate_origin/ dest/
```

`is_up` checks if an IP or domain name is up.

```sh
is_up 0xacab.org && git push
```

`has_internet` save as above but hardcoding google.com:

```sh
has_internet && rsync ...
```

`toggle` enables or disables a topic if the opposite is true.

```sh
toggle sync
```

Combinable
----------

Conditions are combinable:

```sh
has_internet && should sync && hold sync rsync ...
```

Sync at intermitent hours:

```crontab
@hourly toggle sync
```

A failed task disables another:

```crontab
@hourly rsync ... || dont sync
```

Install
-------

As root (or `fakeroot` if you're packaging).

```sh
make install
```
